import sbt.Keys.libraryDependencies

name := "awesome-banking"

organization := "com.github.dnvriend"

version := "1.0.0"

scalaVersion := "2.12.4"

resolvers += Resolver.jcenterRepo

libraryDependencies ++= {
  val akkaVersion = "2.5.11"
  Seq(
    "com.typesafe.akka" %% "akka-actor" % akkaVersion,
    "com.typesafe.akka" %% "akka-slf4j" % akkaVersion,
    "com.typesafe.akka" %% "akka-persistence" % akkaVersion,
    "com.typesafe.akka" %% "akka-stream" % akkaVersion,
    "com.typesafe.akka" %% "akka-remote" % akkaVersion,
    "com.typesafe.akka" %% "akka-cluster" % akkaVersion,
    "com.typesafe.akka" %% "akka-cluster-tools" % akkaVersion,
    "com.typesafe.akka" %% "akka-cluster-sharding" % akkaVersion,
    "com.typesafe.akka" %% "akka-persistence-cassandra" % "0.55",
    "com.typesafe.akka" %% "akka-persistence-cassandra-launcher" % "0.55" % Test,
    "com.typesafe.akka" %% "akka-persistence" % akkaVersion,
    "ch.qos.logback" % "logback-classic" % "1.2.3" % Test,
    "com.typesafe.akka" %% "akka-persistence-query" % "2.5.11",
    "org.scalikejdbc" %% "scalikejdbc"       % "3.2.1",
    "com.h2database"  %  "h2"                % "1.4.196",
    "org.postgresql" % "postgresql" % "9.4-1200-jdbc41",
    "org.scalikejdbc" %% "scalikejdbc-config"  % "3.2.1",
    "org.scalaz" %% "scalaz-core" % "7.2.20",
    "com.github.dnvriend" %% "akka-persistence-inmemory" % "2.5.1.1" % Test,
    "com.typesafe.akka" %% "akka-stream-testkit" % akkaVersion % Test,
    "org.scalatest" %% "scalatest" % "3.0.5" % Test,
    "org.scalacheck" %% "scalacheck" % "1.13.4" % Test,
//  "com.thesamet.scalapb" %% "scalapb-runtime" % scalapb.compiler.Version.scalapbVersion % "protobuf" ,
    "org.scalatestplus.play" %% "scalatestplus-play" % "3.0.0-M3" % Test,
    "com.softwaremill.macwire" %% "macros" % "2.3.0" % "provided",
    "com.typesafe.akka" %% "akka-stream-kafka" % "0.19"
  )
}

fork in Test := true

scalacOptions ++= Seq("-feature", "-language:higherKinds", "-language:implicitConversions", "-deprecation",  "-Ydelambdafy:method", "-target:jvm-1.8")

javaOptions in Test ++= Seq("-Xms30m", "-Xmx30m")

parallelExecution in Test := false

licenses +=("Apache-2.0", url("http://opensource.org/licenses/apache2.0.php"))

organizationName := "Akbar Ahmad"
startYear := Some(2018)
licenses += ("Apache-2.0", new URL("https://www.apache.org/licenses/LICENSE-2.0.txt"))

//lazy val root = (project in file(".")).enablePlugins(PlayScala)
enablePlugins( PlayScala/*, ScalikejdbcPlugin*/)
disablePlugins(PlayLayoutPlugin)

PB.targets in Compile := Seq(
  scalapb.gen() -> (sourceManaged in Compile).value
)

