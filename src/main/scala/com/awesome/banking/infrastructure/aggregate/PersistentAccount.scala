package com.awesome.banking.infrastructure.aggregate

import akka.actor.Props
import akka.persistence.PersistentActor
import com.awesome.banking.domain.model.aggregate.Account
import com.awesome.banking.domain.model.command.{DepositToAccountCommand, OpenAccountCommand, WithdrawFromAccountCommand}
import com.awesome.banking.domain.model.event._
import com.awesome.banking.infrastructure.akka.errors._

object PersistentAccount{
  def props() = Props(new PersistentAccount())
}

class PersistentAccount() extends PersistentActor {

  private var state: Option[Account] = None

  override def receiveRecover: Receive = {
    case event:AccountEvent =>  handleEvent(event)
  }

  override def receiveCommand: Receive = {
    case cmd:OpenAccountCommand => open(cmd)
    case cmd:DepositToAccountCommand => deposit(cmd)
    case cmd:WithdrawFromAccountCommand => withdraw(cmd)
  }

  override def persistenceId: String = s"account-${self.path.name}"

  private def open(command: OpenAccountCommand): Unit = state match {
      case Some(_) =>  sender() ! Failure(FailureType.Validation, AccountAlreadyCreated)
      case None =>   persist(AccountOpened(command.accountId, command.initialBalance,  System.currentTimeMillis()))(handleEventAndRespond)
  }


  private def deposit(command: DepositToAccountCommand) = state match {
    case None =>  sender() ! Failure(FailureType.Validation, AccountDoesNotExist)
    case Some(acc) =>  persist(DepositedToAccount(acc.id, command.amount, command.transactionId, System.currentTimeMillis()))(handleEventAndRespond)
  }

  private def withdraw(command: WithdrawFromAccountCommand)= state match {
    case None =>  sender() ! Failure(FailureType.Validation, AccountDoesNotExist)
    case Some(acc) =>
      if (acc.balance < command.amount)
        sender() ! Failure(FailureType.Validation, BalanceNotEnough)
      else
      persist(WithdrawnFromAccount(acc.id, command.amount, command.transactionId, System.currentTimeMillis()))(handleEventAndRespond)
  }

  def handleEventAndRespond(event:Event):Unit = {
    handleEvent(event)
    sender() ! stateResponse()
  }

  def stateResponse() =
    if (state.isEmpty) EmptyResult else FullResult(state)

  def handleEvent(event:Event):Unit = event match {
    case AccountOpened(accountId: String, initialBalance : BigDecimal, _) =>
      state = Option(Account(accountId,initialBalance))

    case DepositedToAccount(accountId, deposit, transactionId, _) =>
      state = state.map (acc => acc.copy(balance = acc.balance + deposit))

    case WithdrawnFromAccount(accountId, withdrawnAmount, transactionId, _) =>
      state = state.map (acc => acc.copy(balance = acc.balance - withdrawnAmount))
  }

  val AccountAlreadyCreated = ErrorMessage("account.alreadyexists", Some("This account has already been opened and can not handle another OpenAccount request"))

  val AccountDoesNotExist = ErrorMessage("account.doesnotexist", Some("This account does not exist and can not handle a transaction"))

  val BalanceNotEnough = ErrorMessage("account.notenoughbalance", Some("This account does not contain enough balance to withdraw desired amount"))

}
