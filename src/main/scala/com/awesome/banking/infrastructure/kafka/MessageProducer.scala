package com.awesome.banking.infrastructure.kafka

import java.io.{ByteArrayOutputStream, ObjectOutputStream}

import akka.actor.{Actor, ActorLogging, Props}
import akka.kafka.scaladsl.Producer
import akka.kafka.{ProducerMessage, ProducerSettings}
import akka.pattern.pipe
import akka.persistence.cassandra.query.scaladsl.CassandraReadJournal
import akka.persistence.query.{EventEnvelope, Offset, PersistenceQuery, TimeBasedUUID}
import akka.stream.scaladsl.{Flow, Sink}
import akka.stream.{ActorMaterializer, ActorMaterializerSettings, Supervision}
import com.awesome.banking.domain.model.event.{AccountOpened, DepositedToAccount, WithdrawnFromAccount}
import com.awesome.banking.infrastructure.converter.Convert
import com.awesome.banking.infrastructure.view.ResumableProjection
import com.awesome.banking.infrastructure.view.ViewBuilder.LatestOffsetResult
import org.apache.kafka.clients.producer.ProducerRecord

import scala.util.control.NonFatal

object  MessageProducer{
  val Name = "account-message-sender"
   def topicRecord(topic: String, key: String, value: Array[Byte]) = {
    new ProducerRecord[String, Array[Byte]](topic, key, value)
  }

  //TODO : Change it to some other format
  def serialise(value: Any): Array[Byte] = {
    val stream: ByteArrayOutputStream = new ByteArrayOutputStream()
    val oos = new ObjectOutputStream(stream)
    oos.writeObject(value)
    oos.close()
    stream.toByteArray
  }


  def props(ps:ProducerSettings[String,Array[Byte]]) = Props(new MessageProducer(ps))
}
class MessageProducer(ps:ProducerSettings[String,Array[Byte]]) extends Actor with ActorLogging {
  import context.dispatcher
  //Set up the persistence query to listen for events for the target entity type
  val journal = PersistenceQuery(context.system).
    readJournalFor[CassandraReadJournal](CassandraReadJournal.Identifier)



  val decider: Supervision.Decider = {
    case NonFatal(ex) =>
      log.error(ex, "Got non fatal exception in MessageProducer flow")
      Supervision.Resume
    case ex  =>
      log.error(ex, "Got fatal exception in MessageProducer flow, stream will be stopped")
      Supervision.Stop
  }
  implicit val materializer = ActorMaterializer(
    ActorMaterializerSettings(context.system).
      withSupervisionStrategy(decider)
  )
  def projectionId = "account-producer"
  import com.awesome.banking.proto.events.{AccountOpened => ProtoAccountOpened, DepositedToAccount => ProtoDepositedToAccount, WithdrawnFromAccount => ProtoWithdrawnFromAccountToAccount}

  lazy val eventsFlow =
    Flow[EventEnvelope].
      map{ env =>
        val id = env.persistenceId.toLowerCase().drop("account".length() + 1)
        val envelope = env.event match {
          case a @ AccountOpened(accountId: String, initialBalance : BigDecimal, occurredOn:Long)  =>
            val converted = Convert.accountOpenedToProto(a)
            val envelope = MessageEnvelope[ProtoAccountOpened] (
            id = id,
            nameSpace = converted.getClass.getName,
            version = 1,
            message = converted
        )
            envelope
          case d @ DepositedToAccount(accountId, deposit, _, _) =>
            val converted = Convert.depositedToAccountToProto(d)
            val envelope = MessageEnvelope[ProtoDepositedToAccount] (
              id = id,
              nameSpace = converted.getClass.getPackage.getName,
              version = 1,
              message = converted
            )
            envelope
          case w @ WithdrawnFromAccount(accountId, withdrawnAmount, _, _) =>
            val converted = Convert.withdrawnFromAccountToProto(w)
            val envelope = MessageEnvelope[ProtoWithdrawnFromAccountToAccount] (
              id = id,
              nameSpace = converted.getClass.getPackage.getName,
              version = 1,
              message = converted
            )
            envelope
        }
        val tr = MessageProducer.topicRecord("account", envelope.id, MessageProducer.serialise(envelope))
        ProducerMessage.Message(tr,env)
      }
      .via(Producer.flow(ps))
      .mapAsync(1)(res => resumableProjection.storeLatestOffset(res.message.passThrough.offset))

  val resumableProjection = ResumableProjection(projectionId, context.system)

  resumableProjection.
    fetchLatestOffset.
    map(o => LatestOffsetResult(o)).
    pipeTo(self)

  override def receive: Receive = {
    case LatestOffsetResult(o) =>

      val offset: Offset = o.getOrElse(TimeBasedUUID.apply(com.datastax.driver.core.utils.UUIDs.startOf(0L)))

      val eventsSource = journal.eventsByTag("account", offset)
      eventsSource.via(eventsFlow).runWith(Sink.ignore)
  }
}
