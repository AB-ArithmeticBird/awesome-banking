package com.awesome.banking.infrastructure.kafka

case class MessageEnvelope[T](id: String, nameSpace: String, version:Int, message:T)
