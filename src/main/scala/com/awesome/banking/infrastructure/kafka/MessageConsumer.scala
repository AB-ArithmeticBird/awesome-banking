package com.awesome.banking.infrastructure.kafka

import java.io.{ByteArrayInputStream, ObjectInputStream}

import akka.{Done, NotUsed}
import akka.actor.{Actor, ActorLogging, Props}
import akka.kafka.ConsumerMessage.CommittableMessage
import akka.kafka.scaladsl.Consumer
import akka.kafka.{ConsumerSettings, Subscriptions}
import akka.stream.scaladsl.{Flow, Sink}
import akka.stream.{ActorMaterializer, ActorMaterializerSettings, Supervision}
import com.awesome.banking.domain.model.event.AccountEvent
import com.awesome.banking.infrastructure.converter.Convert

import scala.util.control.NonFatal

object MessageConsumer {
  val Name = "account-message-consumer"
  def deserialise(bytes: Array[Byte]): Any = {
    val ois = new ObjectInputStream(new ByteArrayInputStream(bytes))
    val value = ois.readObject
    ois.close()
    value
  }

  def props(cs:ConsumerSettings[String,Array[Byte]]) = Props(new MessageConsumer(cs))
}

class MessageConsumer(cs:ConsumerSettings[String,Array[Byte]]) extends Actor with ActorLogging {

  val decider: Supervision.Decider = {
    case NonFatal(ex) =>
      log.error(ex, "Got non fatal exception in MessageConsumer flow")
      Supervision.Resume
    case ex  =>
      log.error(ex, "Got fatal exception in MessageConsumer flow, stream will be stopped")
      Supervision.Stop
  }
  
  implicit val materializer = ActorMaterializer(
    ActorMaterializerSettings(context.system).
      withSupervisionStrategy(decider)
  )

  source.via(eventsFlow).runWith(Sink.ignore)
  


  private lazy val source = Consumer.committableSource(cs, Subscriptions.topics("account"))

  import com.awesome.banking.proto.events.{AccountOpened => ProtoAccountOpened, DepositedToAccount => ProtoDepositedToAccount, WithdrawnFromAccount => ProtoWithdrawnFromAccountToAccount}

  private lazy val eventsFlow: Flow[CommittableMessage[String, Array[Byte]], Done, NotUsed] =  Flow[CommittableMessage[String, Array[Byte]]]
    .map{e =>
        val msgEnvlp = MessageConsumer.deserialise( e.record.value()).asInstanceOf[MessageEnvelope[_]]
         msgEnvlp.nameSpace match{
          case "com.awesome.banking.proto.events.AccountOpened" => {
            val ao = Convert.protoToAccountOpened(msgEnvlp.message.asInstanceOf[ProtoAccountOpened])
            log.info("AccountId is:" +ao.accountId)
            action(ao)
          }
          case "com.awesome.banking.proto.events.DepositedToAccount" =>
            val da = Convert.protoToDepositedToAccount(msgEnvlp.message.asInstanceOf[ProtoDepositedToAccount])
            action(da)
          case "com.awesome.banking.proto.events.WithdrawnFromAccount" =>
            val wda = Convert.protoToWithdrawnFromAccount(msgEnvlp.message.asInstanceOf[ProtoWithdrawnFromAccountToAccount])
            action(wda)
          case x@_ => log.info("Strange Error" + x)
        }
     e
    }.mapAsync(1) { msg =>
    msg.committableOffset.commitScaladsl()
  }

  private def action (ae:AccountEvent) = {
    ???
  }
  
  override def receive: Receive = {
    case Done =>
      log.info("Runnable graph completed")
  }
}
