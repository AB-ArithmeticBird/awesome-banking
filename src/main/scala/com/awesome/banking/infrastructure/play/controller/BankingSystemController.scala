package com.awesome.banking.infrastructure.play.controller

import java.util.UUID

import com.awesome.banking.domain.model.aggregate.Account
import com.awesome.banking.domain.model.command.{DepositToAccountCommand, OpenAccountCommand, WithdrawFromAccountCommand}
import com.awesome.banking.domain.repository.AccountAlgebra
import com.awesome.banking.domain.service.BankingServiceAlgebra
import com.awesome.banking.infrastructure.akka.errors.{Failure, FullResult}
import com.awesome.banking.infrastructure.play.dto.{AccountDepositView, AccountOpenView, AccountWithdrawView}
import play.api.mvc._
import com.awesome.banking.infrastructure.akka.errors.EmptyResult

import scala.concurrent.ExecutionContext

class BankingSystemController(cc: ControllerComponents, api: BankingServiceAlgebra, acc:AccountAlgebra)(implicit ec: ExecutionContext)
  extends AbstractController(cc){
  def greetings = Action {
    Ok("hello AB")
  }

  def register(): Action[AccountOpenView] = Action.async(parse.json[AccountOpenView]) { implicit req =>
    val m = req.body
    val r = api.openAccount(OpenAccountCommand(m.initialBalance, UUID.randomUUID().toString))
    r.map {
      case f: FullResult[Option[Account]] => Ok("Registration Successful" + f.value.map(_.id).head )
      case EmptyResult => Ok("Registration Failed")
      case Failure(_, _, _) => Ok("Registration Failed")
    }
  }

  def deposit(): Action[AccountDepositView] = Action.async(parse.json[AccountDepositView]) { implicit req =>
    val m = req.body
    val tId = UUID.randomUUID().toString
    val r = api.deposit(DepositToAccountCommand(m.deposit, tId, m.accountId))
    r.map {
      case f: FullResult[Option[Account]] => Ok("deposit Successful for account " + f.value.map(_.id).head  + "with transactionId :" + tId)
      case Failure(failType, message, exception) => Ok("Deposit Failed")
      case EmptyResult => Ok("Deposit Failed")
    }
  }

  def withdraw(): Action[AccountWithdrawView] = Action.async(parse.json[AccountWithdrawView]) { implicit req =>
    val m = req.body
    val tId = UUID.randomUUID().toString
    val r = api.withdraw(WithdrawFromAccountCommand(m.withdraw, tId, m.accountId))
    r.map {
      case f: FullResult[Option[Account]] => Ok("withdraw Successful for account " + f.value.map(_.id).head  + " with transactionId :" + tId)
      case Failure(failType, message, exception) => Ok("Withdraw Failed")
      case EmptyResult => Ok("Withdraw Failed")
    }
  }

//  def transfer(): Action[Transfer] = Action.async(parse.json[Transfer]) { implicit req =>
//    val m = req.body
//    val tId = UUID.randomUUID().toString
//    val r: Future[Result] = api.withdraw(WithdrawFromAccountCommand(m.amount, tId, m.sourceAccountId)).map {
//      case f: FullResult[Option[Account]] =>
//        api.deposit(DepositToAccountCommand(m.amount, tId, m.destinationAccountId))
//        Ok("Transfer Success")
//      case Failure(failType, message, exception) =>
//        Ok("Transfer Failed")
//    }
//       r
//  }

  def account(id:String): Action[AnyContent] = Action.async{ implicit req =>
    val r = acc.getAccount(id)
    r.map {
      case Some(a) => Ok("account balance for accountId " +a.id + " is : " + a.balance)
      case None => Ok("No account found")
    }
  }
}
