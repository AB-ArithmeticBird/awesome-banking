/*
 * Copyright 2017 Akbar Ahmad
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.awesome.banking.infrastructure.play

import java.util.concurrent.TimeUnit

import _root_.controllers.AssetsComponents
import akka.actor.{ActorRef, ActorSystem, PoisonPill, Props}
import akka.cluster.singleton.{ClusterSingletonManager, ClusterSingletonManagerSettings}
import akka.kafka.{ConsumerSettings, ProducerSettings}
import akka.util.Timeout
import com.awesome.banking.domain.repository.AccountAlgebra
import com.awesome.banking.infrastructure.kafka.{MessageConsumer, MessageProducer}
import com.awesome.banking.infrastructure.play.controller.BankingSystemController
import com.awesome.banking.infrastructure.service.{BankingService, ClusterShardAccountLocator}
import com.awesome.banking.infrastructure.view.{AccountRepo, ViewBuilder}
import com.softwaremill.macwire._
import com.typesafe.config.ConfigFactory
import org.apache.kafka.common.serialization.{ByteArrayDeserializer, ByteArraySerializer, StringDeserializer, StringSerializer}
import play.api.ApplicationLoader.Context
import play.api._
import play.api.i18n._
import play.api.routing.Router
import router.Routes
import scalikejdbc.ConnectionPool

class BankingSystemApplicationLoader extends ApplicationLoader {
  override def load(context: ApplicationLoader.Context) = new BankingSystemComponents(context).application
}

class BankingSystemComponents(context: Context) extends BuiltInComponentsFromContext(context)
    with BankingSystemModule
    with AssetsComponents
    with I18nComponents with play.filters.HttpFiltersComponents {

  implicit val system = actorSystem
  lazy val timeOut = Timeout(10000, TimeUnit.MILLISECONDS)
  lazy val movieSeatsLocatorService: ClusterShardAccountLocator = wire[ClusterShardAccountLocator]
  lazy val bankingServiceAlgebra: BankingService = wire[BankingService]
  lazy val accountAlgebra: AccountAlgebra = wire[AccountRepo]

  lazy val numberOfShard = 8
  lazy val bankingSystemController: BankingSystemController = wire[BankingSystemController]

  // set up logger
  LoggerConfigurator(context.environment.classLoader).foreach {
    _.configure(context.environment, context.initialConfiguration, Map.empty)
  }

  lazy val router: Router = {
    // add the prefix string in local scope for the Routes constructor
    val prefix: String = "/"
    wire[Routes]
  }
  val c = ConfigFactory.load()
  ConnectionPool.singleton(c.getString("db.url"), c.getString("db.user"), c.getString("db.pass"))

  lazy val producerSettings: ProducerSettings[String, Array[Byte]] =
    ProducerSettings(system,  Option(new StringSerializer()), Option(new ByteArraySerializer())).withBootstrapServers("localhost:9092")


  lazy val consumerSettings : ConsumerSettings[String, Array[Byte]] =
    ConsumerSettings(system, new StringDeserializer, new ByteArrayDeserializer).withGroupId("group1").withBootstrapServers("localhost:9092")



  startSingleton(system, ViewBuilder.props, ViewBuilder.Name)
  startSingleton(system, MessageProducer.props(producerSettings), MessageProducer.Name)//start the kafka producer
  startSingleton(system, MessageConsumer.props(consumerSettings), MessageConsumer.Name)//start the kafka producer


  def startSingleton(system:ActorSystem, props:Props,
                     managerName:String, terminationMessage:Any = PoisonPill):ActorRef = {

    system.actorOf(
      ClusterSingletonManager.props(
        singletonProps = props,
        terminationMessage = terminationMessage,
        settings = ClusterSingletonManagerSettings(system)),
      managerName)
  }
  
}