package com.awesome.banking.infrastructure.play.dto

import play.api.libs.json.{ OFormat, _ }

case class AccountOpenView(initialBalance: BigDecimal)

object AccountOpenView {
  implicit lazy val accountOpenViewFormat: OFormat[AccountOpenView] =
    Json.format[AccountOpenView]
}

case class AccountDepositView(accountId: String, deposit: BigDecimal)

object AccountDepositView {
  implicit lazy val accountDepositViewFormat: OFormat[AccountDepositView] =
    Json.format[AccountDepositView]
}


case class AccountWithdrawView(accountId: String, withdraw: BigDecimal)

object AccountWithdrawView {
  implicit lazy val accountWithdrawViewFormat: OFormat[AccountWithdrawView] =
    Json.format[AccountWithdrawView]
}

case class Transfer(sourceAccountId: String, destinationAccountId:String, amount: BigDecimal)

object Transfer {
  implicit lazy val transferFormat: OFormat[Transfer] =
    Json.format[Transfer]
}