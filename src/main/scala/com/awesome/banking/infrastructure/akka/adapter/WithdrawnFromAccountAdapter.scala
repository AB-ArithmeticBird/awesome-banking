package com.awesome.banking.infrastructure.akka.adapter

import akka.persistence.journal.{EventAdapter, EventSeq, Tagged}
import com.awesome.banking.domain.model.event.WithdrawnFromAccount
import com.awesome.banking.infrastructure.converter.Convert
import com.awesome.banking.proto.events.{WithdrawnFromAccount => ProtoWithdrawnFromAccountToAccount}

class WithdrawnFromAccountAdapter extends EventAdapter{
  override def fromJournal(event: Any, manifest: String): EventSeq =
    event match {
      case evt: ProtoWithdrawnFromAccountToAccount=>
        EventSeq(
          Convert.protoToWithdrawnFromAccount(evt)
        )
    }

  override def manifest(event: Any): String = event.getClass.getName

  override def toJournal(event: Any): Any = event match {
    case evt: WithdrawnFromAccount =>
      Tagged(Convert.withdrawnFromAccountToProto(evt), Set("account"))
  }
}
