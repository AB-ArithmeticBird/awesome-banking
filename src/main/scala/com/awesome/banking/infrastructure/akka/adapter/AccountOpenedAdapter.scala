package com.awesome.banking.infrastructure.akka.adapter

import akka.persistence.journal.{EventAdapter, EventSeq, Tagged}
import com.awesome.banking.domain.model.event.AccountOpened
import com.awesome.banking.infrastructure.converter.Convert
import com.awesome.banking.proto.events.{AccountOpened => ProtoAccountOpened}


class AccountOpenedAdapter extends EventAdapter {
  override def fromJournal(event: Any, manifest: String): EventSeq =
    event match {
      case evt: ProtoAccountOpened=>
        EventSeq(
          Convert.protoToAccountOpened(evt)
        )
    }

  override def manifest(event: Any): String = event.getClass.getName

  override def toJournal(event: Any): Any = event match {
    case evt: AccountOpened =>
      Tagged(Convert.accountOpenedToProto(evt), Set("account"))
  }
}
