package com.awesome.banking.infrastructure.akka.adapter

import akka.persistence.journal.{EventAdapter, EventSeq, Tagged}
import com.awesome.banking.domain.model.event.DepositedToAccount
import com.awesome.banking.infrastructure.converter.Convert
import com.awesome.banking.proto.events.{DepositedToAccount => ProtoDepositedToAccount}

class DepositedToAccountAdapter extends EventAdapter{
  override def fromJournal(event: Any, manifest: String): EventSeq =
    event match {
      case evt: ProtoDepositedToAccount=>
        EventSeq(
          Convert.protoToDepositedToAccount(evt)
        )
    }

  override def manifest(event: Any): String = event.getClass.getName

  override def toJournal(event: Any): Any = event match {
    case evt: DepositedToAccount =>
      Tagged(Convert.depositedToAccountToProto(evt), Set("account"))
  }
}
