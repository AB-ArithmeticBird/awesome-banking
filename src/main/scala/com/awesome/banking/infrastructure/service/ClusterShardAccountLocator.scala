package com.awesome.banking.infrastructure.service

import akka.actor.{ActorRef, ActorSystem}
import akka.cluster.Cluster
import akka.cluster.sharding.{ClusterSharding, ClusterShardingSettings, ShardRegion}
import com.awesome.banking.domain.model.command.Command
import com.awesome.banking.domain.service.AccountLocator
import com.awesome.banking.infrastructure.aggregate.PersistentAccount

class ClusterShardAccountLocator(numberOfShards: Int)(implicit actorSystem: ActorSystem) extends AccountLocator{
  val entityTypeName: String = "AccountCache"

  private val extractEntityId: ShardRegion.ExtractEntityId = {
    case msg: Command => (msg.entityId, msg)
  }

  val extractShardId: ShardRegion.ExtractShardId = {
    case msg: Command => (msg.entityId.hashCode % numberOfShards).toString
  }

  private def initNewShard(system: ActorSystem = actorSystem): ActorRef = {
    ClusterSharding(system).start(
      typeName = entityTypeName,
      entityProps = PersistentAccount.props(),
      settings = ClusterShardingSettings(system),
      extractEntityId = extractEntityId,
      extractShardId = extractShardId
    )
  }

  private val _account: ActorRef = initNewShard(actorSystem)


  override def account() = _account

  def leaveCluster(): Unit = {
    _account ! ShardRegion.GracefulShutdown
    val cluster = Cluster(actorSystem)
    cluster.leave(cluster.selfAddress)
    println("Shutting down!!!")
  }

}
