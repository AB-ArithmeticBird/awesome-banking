package com.awesome.banking.infrastructure.service

import _root_.akka.pattern.ask
import com.awesome.banking.domain.model.aggregate.Account
import com.awesome.banking.domain.model.command.{DepositToAccountCommand, OpenAccountCommand, WithdrawFromAccountCommand}
import com.awesome.banking.domain.service.BankingServiceAlgebra
import com.awesome.banking.infrastructure.akka.errors.ServiceResult

import scala.concurrent.Future

class BankingService (
                       locator: ClusterShardAccountLocator,
                       timeout: _root_.akka.util.Timeout
                     ) extends BankingServiceAlgebra {
  private implicit val askTimeout = timeout


  override def openAccount(command: OpenAccountCommand): Future[ServiceResult[Option[Account]]] = {
  (locator.account() ? command). mapTo[ServiceResult[Option[Account]]]
}

  override def deposit(command: DepositToAccountCommand): Future[ServiceResult[Option[Account]]] = {
    (locator.account() ? command). mapTo[ServiceResult[Option[Account]]]
  }

  override def withdraw(command: WithdrawFromAccountCommand):Future[ServiceResult[Option[Account]]] = {
    (locator.account() ? command). mapTo[ServiceResult[Option[Account]]]
  }
}