package com.awesome.banking.infrastructure.view

import com.awesome.banking.domain.model.aggregate.Account
import com.awesome.banking.domain.repository.AccountAlgebra
import scalikejdbc._

import scala.concurrent.{ExecutionContext, Future}

class AccountRepo extends AccountAlgebra{
  override def getAccount(id: String)(implicit ec: ExecutionContext): Future[Option[Account]] =  Future{
    DB localTx { implicit session =>
      sql"select * from account where accountid = ${id}".map(rs => Account(rs.string("accountid"), rs.get[Double]("balance"))).single.apply()
    }
  }
}
