package com.awesome.banking.infrastructure.view

import java.util.UUID
import java.util.concurrent.atomic.AtomicBoolean

import akka.actor.{ActorSystem, ExtendedActorSystem, Extension, ExtensionId, ExtensionIdProvider}
import akka.event.Logging
import akka.persistence.query.{Offset, TimeBasedUUID}
import com.datastax.driver.core.Session

import scala.concurrent.Future

abstract class ResumableProjection(identifier:String) {
  def storeLatestOffset(offset:Offset):Future[Boolean]
  def fetchLatestOffset:Future[Option[Offset]]
}

object ResumableProjection{
  def apply(identifier:String, system:ActorSystem) =
    new CassandraResumableProjection(identifier, system)
}

class CassandraResumableProjection(identifier:String, system:ActorSystem) extends ResumableProjection(identifier){
  import system.dispatcher
  val projectionStorage = CassandraProjectionStorage(system)

  def storeLatestOffset(offset:Offset):Future[Boolean] = {
    offset match {
      case t:TimeBasedUUID => projectionStorage.updateOffset(identifier, t.value)
      case _ =>   throw new Exception("Not supported")
    }

  }
  def fetchLatestOffset:Future[Option[Offset]] = {
    projectionStorage.fetchLatestOffset(identifier).map(o => o.map(oo =>Offset.timeBasedUUID(oo)))
  }
}

class CassandraProjectionStorageExt(system:ActorSystem) extends Extension {
  import akka.persistence.cassandra.listenableFutureToFuture
  import system.dispatcher

  val cassandraConfig = system.settings.config.getConfig("cassandra")
  implicit val log = Logging(system.eventStream, "CassandraProjectionStorage")

  var initialized = new AtomicBoolean(false)
  val createKeyspaceStmt = """
      CREATE KEYSPACE IF NOT EXISTS bookstore
      WITH REPLICATION = { 'class' : 'SimpleStrategy', 'replication_factor' : 1 }
    """

  val createTableStmt = """
      CREATE TABLE IF NOT EXISTS bookstore.projectionoffsets (
        identifier varchar primary key, offset uuid)
  """

  val init: Session => Future[Unit] = (session: Session) => for {
    _ <- session.executeAsync(createKeyspaceStmt)
    _ <- session.executeAsync(createTableStmt)
  } yield ()

  val session = new CassandraSession(system, cassandraConfig, init)

  def updateOffset(identifier:String, offset:UUID): Future[Boolean] = (for {
    session <- session.underlying()
    _ <- session.executeAsync(s"update bookstore.projectionoffsets set offset = ${offset.toString} where identifier = '$identifier'")
  } yield true) recover { case t => false }

  def fetchLatestOffset(identifier:String): Future[Option[UUID]] = for {
    session <- session.underlying()
    rs <- session.executeAsync(s"select offset from bookstore.projectionoffsets where identifier = '$identifier'")
  } yield {
    import collection.JavaConverters._
    rs.all().asScala.headOption.map(_.getUUID(0))
  }
}
object CassandraProjectionStorage extends ExtensionId[CassandraProjectionStorageExt] with ExtensionIdProvider {
  override def lookup = CassandraProjectionStorage
  override def createExtension(system: ExtendedActorSystem) =
    new CassandraProjectionStorageExt(system)
}