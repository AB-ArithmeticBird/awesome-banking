package com.awesome.banking.infrastructure.view

import akka.actor.{Actor, ActorLogging, Props}
import akka.pattern.pipe
import akka.persistence.cassandra.query.scaladsl.CassandraReadJournal
import akka.persistence.query.{EventEnvelope, Offset, PersistenceQuery, TimeBasedUUID}
import akka.stream.scaladsl.{Flow, Sink, Source}
import akka.stream.{ActorMaterializer, ActorMaterializerSettings, Supervision}
import com.awesome.banking.domain.model.event.{AccountOpened, DepositedToAccount, WithdrawnFromAccount}
import com.awesome.banking.infrastructure.view.ViewBuilder._

import scala.concurrent.Future
import scala.util.control.NonFatal

object ViewBuilder{
  case class AccountReadModel(accountId:String, balance: BigDecimal)

  sealed trait ViewAction
  case class InsertAction(id:String, rm:AccountReadModel) extends ViewAction
  case class UpdateAction(id:String, action: String, amount:BigDecimal) extends ViewAction
  case class NoAction(id:String) extends ViewAction
  case class DeferredCreate(flow:Flow[EnvelopeAndAction,EnvelopeAndAction,akka.NotUsed]) extends ViewAction

  case class EnvelopeAndFunction(env:EventEnvelope, f: () => Future[Unit])

  case class LatestOffsetResult(offset:Option[Offset])
  case class EnvelopeAndAction(env:EventEnvelope, action:ViewAction)

  val Name = "account-view-builder"
  def props = Props[ViewBuilder]
}
class ViewBuilder extends Actor with ActorLogging {

  import context.dispatcher
  //Set up the persistence query to listen for events for the target entity type
  val journal = PersistenceQuery(context.system).
    readJournalFor[CassandraReadJournal](CassandraReadJournal.Identifier)


  val decider: Supervision.Decider = {
    case NonFatal(ex) =>
      log.error(ex, "Got non fatal exception in ViewBuilder flow")
      Supervision.Resume
    case ex  =>
      log.error(ex, "Got fatal exception in ViewBuilder flow, stream will be stopped")
      Supervision.Stop
  }
  implicit val materializer = ActorMaterializer(
    ActorMaterializerSettings(context.system).
      withSupervisionStrategy(decider)
  )

  val resumableProjection = ResumableProjection(projectionId, context.system)
  resumableProjection.
    fetchLatestOffset.
    map(o => LatestOffsetResult(o)).
    pipeTo(self)

  def projectionId = "account-view-builder"

  def actionFor(bookId:String, env:EventEnvelope) = env.event match {
    case AccountOpened(accountId: String, initialBalance : BigDecimal, occurredOn:Long) =>
      log.info("Saving a new book entity into the postgres : {}", accountId)
      val accountRM = AccountReadModel(accountId, initialBalance )
      InsertAction(accountId, accountRM)
    case DepositedToAccount(accountId, deposit, _, _) =>
      UpdateAction(accountId, "add", deposit)
    case WithdrawnFromAccount(accountId, withdrawnAmount, _, _) =>
      UpdateAction(accountId, "sub", withdrawnAmount)
  }
  val eventsFlow =
    Flow[EventEnvelope].
      map{ env =>
        val id = env.persistenceId.toLowerCase().drop("account".length() + 1)
        EnvelopeAndAction(env, actionFor(id, env))
      }.
      flatMapConcat{
        case ea @ EnvelopeAndAction(env, cr:DeferredCreate) =>
          Source.single(ea).via(cr.flow )

        case ea:EnvelopeAndAction =>
          Source.single(ea).via(Flow[EnvelopeAndAction])
      }.
      collect{
        case EnvelopeAndAction(env, InsertAction(id, rm:AccountReadModel)) =>
          EnvelopeAndFunction(env, () => insert(id, rm))

        case EnvelopeAndAction(env, u:UpdateAction) =>
          EnvelopeAndFunction(env, () => updateAmount(u.id, u.action, u.amount))

      }.
      mapAsync(1){
        case EnvelopeAndFunction(env, f) => f.apply.map(_ => env)
      }.
      mapAsync(1)(env => resumableProjection.storeLatestOffset(env.offset))

  def insert(id:String, rm: AccountReadModel):Future[Unit] = Future{
    import scalikejdbc._
    val balance = rm.balance
  
    DB localTx{implicit session =>
      sql"INSERT INTO account (accountid, balance) VALUES (${id}, ${balance})".update.apply()
    }
  }

  def updateAmount(id:String, action:String, amount: BigDecimal): Future[Unit] = Future {
    import scalikejdbc._

    DB localTx{implicit session =>
      if (action == "add")
        sql"UPDATE account SET balance = balance + ${amount} where accountid = ${id}".update.apply()
      else
        sql"UPDATE account SET balance = balance - ${amount} where accountid = ${id}".update.apply()
    }
  }

  def clear() = {
    import scalikejdbc._

    DB localTx{implicit session =>
        sql"Truncate account".update.apply()
    }
  }

  override def receive: Receive = {
    case LatestOffsetResult(o) =>
      if (o.isEmpty) clear()
      val offset: Offset = o.getOrElse(TimeBasedUUID.apply(com.datastax.driver.core.utils.UUIDs.startOf(0L)))

      val eventsSource = journal.eventsByTag("account", offset)
      eventsSource.via(eventsFlow).runWith(Sink.ignore)
  }
}
