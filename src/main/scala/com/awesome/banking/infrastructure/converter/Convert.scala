package com.awesome.banking.infrastructure.converter

import com.awesome.banking.domain.model.event.{AccountOpened, DepositedToAccount, WithdrawnFromAccount}
import com.awesome.banking.proto.events.{AccountOpened => ProtoAccountOpened, DepositedToAccount => ProtoDepositedToAccount, WithdrawnFromAccount => ProtoWithdrawnFromAccountToAccount}

object Convert {

  def accountOpenedToProto(accountOpened: AccountOpened): ProtoAccountOpened = {
    ProtoAccountOpened.apply(
      accountId = accountOpened.accountId,
      initialBalance = accountOpened.initialBalance.toString(),
      occurredOn = accountOpened.occurredOn
    )
  }

  def protoToAccountOpened(proto: ProtoAccountOpened):AccountOpened = {
    AccountOpened(
      accountId = proto.accountId,
      initialBalance = BigDecimal(proto.initialBalance),
      occurredOn = proto.occurredOn
    )
  }

  def depositedToAccountToProto(depositedToAccount: DepositedToAccount):ProtoDepositedToAccount = {
    ProtoDepositedToAccount.apply(
      accountId = depositedToAccount.accountId,
      deposit = depositedToAccount.deposit.toString(),
      transactionId = depositedToAccount.transactionId,
      occurredOn = depositedToAccount.occurredOn
    )
  }

  def protoToDepositedToAccount(proto: ProtoDepositedToAccount):DepositedToAccount = {
    DepositedToAccount(
      accountId = proto.accountId,
      deposit = BigDecimal(proto.deposit),
      transactionId = proto.transactionId,
      occurredOn = proto.occurredOn
    )
  }

  def withdrawnFromAccountToProto(withdrawnFromAccount: WithdrawnFromAccount):ProtoWithdrawnFromAccountToAccount = {
    ProtoWithdrawnFromAccountToAccount.apply(
      accountId = withdrawnFromAccount.accountId,
      withdrawnAmount = withdrawnFromAccount.withdrawnAmount.toString(),
      transactionId =  withdrawnFromAccount.transactionId,
      occurredOn = withdrawnFromAccount.occurredOn
    )
  }

  def protoToWithdrawnFromAccount(proto: ProtoWithdrawnFromAccountToAccount): WithdrawnFromAccount = {
    WithdrawnFromAccount(
      accountId = proto.accountId,
      withdrawnAmount = BigDecimal(proto.withdrawnAmount),
      transactionId =  proto.transactionId,
      occurredOn = proto.occurredOn
    )
  }
}
