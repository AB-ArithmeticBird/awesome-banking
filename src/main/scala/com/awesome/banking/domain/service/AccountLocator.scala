package com.awesome.banking.domain.service

import akka.actor.ActorRef

trait AccountLocator {
  def account(): ActorRef
}
