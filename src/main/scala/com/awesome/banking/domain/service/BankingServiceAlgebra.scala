package com.awesome.banking.domain.service

import com.awesome.banking.domain.model.aggregate.Account
import com.awesome.banking.domain.model.command.{DepositToAccountCommand, OpenAccountCommand, WithdrawFromAccountCommand}
import com.awesome.banking.infrastructure.akka.errors.ServiceResult

import scala.concurrent.Future

trait BankingServiceAlgebra {
  def openAccount(command: OpenAccountCommand): Future[ServiceResult[Option[Account]]]

  def deposit(cmd: DepositToAccountCommand): Future[ServiceResult[Option[Account]]]

  def withdraw(cmd: WithdrawFromAccountCommand): Future[ServiceResult[Option[Account]]]
}
