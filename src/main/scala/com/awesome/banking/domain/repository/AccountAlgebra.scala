package com.awesome.banking.domain.repository



import com.awesome.banking.domain.model.aggregate.Account

import scala.concurrent.{ExecutionContext, Future}

trait AccountAlgebra {
      def getAccount(id:String)(implicit ec: ExecutionContext):Future[Option[Account]]
}
