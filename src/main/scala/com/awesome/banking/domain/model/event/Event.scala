package com.awesome.banking.domain.model.event

sealed trait Event  {
  def entityType:String
  def occurredOn:Long
}

sealed trait AccountEvent extends Event  {def entityType = "account"}

final case class AccountOpened(accountId: String, initialBalance : BigDecimal, occurredOn:Long) extends AccountEvent

final case class DepositedToAccount(accountId: String, deposit : BigDecimal, transactionId:String, occurredOn:Long) extends AccountEvent

final case class WithdrawnFromAccount(accountId: String, withdrawnAmount : BigDecimal, transactionId:String, occurredOn:Long) extends AccountEvent

