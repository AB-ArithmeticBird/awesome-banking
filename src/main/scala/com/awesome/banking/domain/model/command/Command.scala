package com.awesome.banking.domain.model.command

trait Command  {
  def entityId:String
}

sealed trait AccountCommand extends Command with Product with Serializable

final case class OpenAccountCommand(initialBalance : BigDecimal, accountId: String) extends AccountCommand {
  override def entityId: String = accountId
}

final case class DepositToAccountCommand(amount:BigDecimal, transactionId: String, accountId: String) extends AccountCommand {
  override def entityId: String = accountId
}

final case class WithdrawFromAccountCommand(amount:BigDecimal, transactionId: String, accountId: String) extends AccountCommand {
  override def entityId: String = accountId
}

