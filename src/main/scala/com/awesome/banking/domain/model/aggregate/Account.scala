package com.awesome.banking.domain.model.aggregate

case class Account (id:String, balance: BigDecimal)