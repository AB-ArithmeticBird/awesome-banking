package com.awesome.bankingsystem.domain

import java.util.UUID

import akka.actor.ActorSystem
import akka.testkit.{ImplicitSender, TestKit}
import com.awesome.banking.domain.model.aggregate.Account
import com.awesome.banking.domain.model.command.{DepositToAccountCommand, OpenAccountCommand, WithdrawFromAccountCommand}
import com.awesome.banking.infrastructure.aggregate.PersistentAccount
import com.awesome.banking.infrastructure.akka.errors.FullResult
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}


class AccountActorSpec extends TestKit(ActorSystem("AccountActorSpec")) with WordSpecLike
  with Matchers
  with BeforeAndAfterAll
  with ImplicitSender {
  override def afterAll: Unit = {
    TestKit.shutdownActorSystem(system)
  }

  "AccountActor" should {
    "opened an account" in {
      val accountActor = system.actorOf(PersistentAccount.props)

      val accountId = UUID.randomUUID().toString
      accountActor ! OpenAccountCommand(123, accountId)
        expectMsg (FullResult(Option(Account(accountId, 123))))
    }

    "deposit in to an account" in {
      val accountActor = system.actorOf(PersistentAccount.props)

      val accountId = UUID.randomUUID().toString
      accountActor ! OpenAccountCommand(123, accountId)
      expectMsg (FullResult(Option(Account(accountId, 123))))

      val tId =  UUID.randomUUID().toString
      accountActor ! DepositToAccountCommand(23, tId, accountId)
      expectMsg (FullResult(Option(Account(accountId, 146))))
    }

    "withdraw from an account" in {
      val accountActor = system.actorOf(PersistentAccount.props)

      val accountId = UUID.randomUUID().toString
      accountActor ! OpenAccountCommand(123, accountId)
      expectMsg (FullResult(Option(Account(accountId, 123))))

      val tId =  UUID.randomUUID().toString
      accountActor ! WithdrawFromAccountCommand(23, tId, accountId)
      expectMsg (FullResult(Option(Account(accountId, 100))))
    }

  }

}
