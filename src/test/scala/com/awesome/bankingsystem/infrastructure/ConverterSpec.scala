package com.awesome.bankingsystem.infrastructure

import com.awesome.banking.domain.model.event.{AccountOpened, DepositedToAccount, WithdrawnFromAccount}
import com.awesome.banking.infrastructure.converter.Convert
import org.scalatest.{FlatSpec, Matchers}

class ConverterSpec extends FlatSpec with Matchers {
  "AccountOpened" should "be correctly converting from and to proto" in {
    val accountOpened = AccountOpened("K1", 100, 1111)
    val res = Convert.protoToAccountOpened(Convert.accountOpenedToProto(accountOpened))
    res should be(accountOpened)
  }

  "DepositedToAccount" should "be correctly converting from and to proto" in {
    val da = DepositedToAccount("K1", 123, "11", 1111)
    val res = Convert.protoToDepositedToAccount(Convert.depositedToAccountToProto(da))
    res should be(da)
  }

  "WithdrawnFromAccount" should "be correctly converting from and to proto" in {
    val da = WithdrawnFromAccount("K1", 123, "11", 1111)
    val res = Convert.protoToWithdrawnFromAccount(Convert.withdrawnFromAccountToProto(da))
    res should be(da)
  }
}
